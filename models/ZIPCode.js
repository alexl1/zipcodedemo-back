const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let ZIPCodeSchema = new Schema({
    code: {
        type: Number,
        required: true
    },
    lat: {
        required: true,
        type: Number
    },
    lng: {
        required: true,
        type: Number
    },
    center: [Number],
    state: String,
    geometry: String,
    createdAt: {
        type: Date,
        default: Date.now
    }
});

let ZipCode = mongoose.model('zipCode', ZIPCodeSchema);

module.exports = ZipCode;
