const config = require('./config');
const express = require('express');
const app = express();
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
const routes = require('./routes');
const cors = require('cors');

/**
 * Creates server by http
 * @type {Server}
 */
const httpServer = http.createServer(app);

/**
 * Adds bodyParser middleware to parse incoming request bodies
 */
app.use(bodyParser.urlencoded({
    limit: "250mb",
    extended: true
}));
app.use(bodyParser.json({limit: "250mb"}));

/**
 * Enables cors middleware for cors requests
 */
app.use(cors());

app.use('/', routes);

/**
 * Set static folder
 */
app.use(express.static(path.join(__dirname, '../app')));
app.use("*", function (req, res) {
    res.sendFile(path.join(__dirname, "../app/index.html"));
});

httpServer.listen(config.port, () => {
    console.log(`http server running at port ${config.port}`);
});
/**
 * Connect to the MongoDB by Mongoose
 */
require('./config/mongoose');
