const router = require('express').Router();
const ZIP = require('../models/ZIPCode');

router.get('/codes', (req, res) => {
    /**
     * Parse request coordinates into GEOJson format
     */
    const shape = req.query.cords ?
                    req.query.cords
                        .split(';')
                        .map(points => points.split(',').map(point => parseFloat(point)).reverse()) : [];

    if ( !shape.length )
        return res.json({
            status: false,
            message: 'Request must contain coordinates of shape'
        });

    /**
     * Adds first coordinate to the end of shape in order to close it
     */
    shape.push(shape[0]);

    /**
     * Searches for coordinates that fall into the polygon
     */
    ZIP.find({
        center: {
            $geoWithin: {
                $geometry: {
                    type: "Polygon",
                    coordinates: [shape]
                }
            }
        }
    }, (err, zips) => {
        if ( err ) {
            return res.json({
                status: false,
                message: 'Error',
                err: err
            })
        }
        res.json({
            status: true,
            data: zips
        })
    });
});

module.exports = router;
