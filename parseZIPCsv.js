require('./config/mongoose');
const ZIP = require('./models/ZIPCode');
const fs = require('fs');
const async = require('async');

fs.readFile('./zipCodes.csv', 'utf8', (err, contents) => {
    if ( err ) {
        throw err;
    }
    let counter = 0;
    const codes = contents.split('\r\n').map(code => code.split(';')).splice(1);

    async.eachSeries(codes, (code, callback) => {
        new ZIP({
            code: parseInt(code[1]),
            lat: parseFloat(code[2]),
            lng: parseFloat(code[4]),
            center: [parseFloat(code[4]), parseFloat(code[2])],
            state: code[3],
            geometry: code[5].match('<MultiGeometry>') ?
                        code[5]
                            .split('<Polygon>')
                            .splice(1)
                            .map(geom => geom.replace(/\//g,'')
                                                        .replace(/[^0-9.,-/\s]/g, ''))
                            .join(';') :
                        code[5]
                            .replace(/\//g,'')
                            .replace(/[^0-9.,-/\s]/g, '')
        }).save( err => {
            if ( err ) {
                throw err;
            }
            counter++;
            console.log(`${counter} of ${codes.length} codes saved`);
            callback();
        })
    }, () => {
        console.log('All codes are saved');
    });
});
