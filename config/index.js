let host = process.env.DBHOST || 'localhost';

const Config = {
    port: 3009,
    mongoose: {
        uri: "mongodb://" + host + "/zip-demo"
    }
};

module.exports = Config;
