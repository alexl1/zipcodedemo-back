const mongoose = require("mongoose");

const config = require("./index");

mongoose.connect(config.mongoose.uri, {useNewUrlParser: true});

module.exports = mongoose;
